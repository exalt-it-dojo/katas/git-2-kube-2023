---
layout: home
permalink: /
---

![Logo Git2Kube](/media-assets/logo-Git2Kube.png "Git2kube")


## Git2Kube, Pipelines automation

Git2Kube est une formation certifiante à la mise en œuvre d'une chaîne de complète de déploiement continue automatisée.

Elle va depuis le code source applicatif dans Git à l'orchestration sur la plateforme de containers Kubernetes.

Elle couvre les aspects modernes du Release Management:

- [12factors](https://12factor.net/),
- [gitflow](https://nvie.com/posts/a-successful-git-branching-model/),
- [DevOps](https://www.youtube.com/watch?v=LdOe18KhtT4).

Elle esquisse les grands principes de la sécurité de production (gestion des credentials et des secrets, ``pull-from-registry`` vs ``push to prod``).

Le fond méthodologique de la formation n'est pas orienté en faveur d'un outil ou d'un cloud provider.

En revanche, les participants à la formation emploient et manipulent des logiciels ou des services de référence :

- [Gitlab](https://gitlab.com/) pour son dépôt, ses pipelines d'automatisation et sa Docker registry,
- [Docker](https://www.docker.com/) pour la containérisation des artifacts,
- [Helm](https://helm.sh/) pour le packaging des pods et la livraison K8S,
- [Kubernetes](https://kubernetes.io/) pour l'orchestration.


## A qui s'adresse la certification Git2Kube ?

Git2Kube s'inscrit dans la philosophie DevOps et ne vise pas un public spécifique, bien au contraire. Elle s'adresse :
- à des profils techniques : Développeurs, Lead dev, Ingénieur système, Admin Sys, Ops.
- ou encore à des profils fonctionnels (Manager, Product Owner ou CDP) qui touchent à la technique.


## Niveau de difficulté de Git2Kube

Git2Kube a été conçue pour satisfaire des participants de tous niveaux :

- Un débutant consolidera un premier socle de compétences conceptuelles et pratiques avec la réalisation de son projet.

- Des profils seniors ou expérimentés pourront confronter leur expériences, décrypter l'infrastructure dédiée à la formation et coacher les plus débutants dans la réalisation de leurs projets (revue par les pairs).


## Une infrastructure AWS

Afin de satisfaire aux meilleures exigences de qualité et de sécurité sanitaire, les sessions de formation Git2Kube se déroulent dans un environnement distanciel sécurisé et conçu spécifiquement :

1. Un groupe de projets Gitlab,
2. Une infrastructure AWS dédiée à la session de formation,
3. Un environnement de développement [AWS Cloud9](https://aws.amazon.com/fr/cloud9/),

> En attendant une partager le code et la config de notre infra de formation, nous [expliquons notre architecture ici](learning-ressources/understanding-Git2Kube-infrastructure-and-tools.md).


## Comment s'obtient la certification Git2Kube ?

La certification Git2Kube s'obtient à l'issue des 2 jours de formation, par le rendu final d'un repo individuel avec en particulier un ``gitlab-ci.yml``, un ``Dockerfile`` et une ``chart`` helm.

A l'issue de la formation, la pipeline doit fonctionner et dispatcher en fonction des tags de release, les rc en staging et les releases vers la prod.

L'historisation des commits successifs des participants attestent des rendus et des livraisons intermédiaires.

## Objectifs pédagogiques, quels seront les acquis de ma formation ?

A l'issue de vos 2 jours de formation Git2Kube, les compétences méthodologiques et techniques :

1. La mise en oeuvre de Gitlab (repository, registry et pipelines), Docker, Helm et Kubernetes
2. La découverte ou la redécouverte des maillons essentielles d'une chaîne de déploiement continue : Artifact Build, Image build, Packaging, Deploy...

# Git2Kube, Libre accès à la connaissance !

Le libre accès à la connaissance fait battre le coeur de Git2Kube !

[Nos supports et le code source de nos exercices sont dispos en CC Partage dans les mêmes conditions](README.md). 

Profitez en pour vous faire une idée de la [pédagogique pratique de Git2Kube](/learning-ressources/).


## Crédits, liens et références

|  Ressource | Description |
| :---- | :---- |
| eXalt IT| Git2Kube est un projet d'[eXalt IT, la filiale dev, cloud & data du groupe eXalt.](https://www.exalt-company.com/nos-offres/exalt-it-dev-cloud-data/) |
| Nicolas & Julien, les auteurs |  Git2Kube a été conçue par Nicolas et Julien, ses 2 auteurs : <br><br>- [Nicolas LACOURTE](https://https://www.linkedin.com/in/nicolaslacourte/), Nicolas est le lead instructeur, l'architecte méthodologique et technique de Git2Kube. Il a conçu l'infrastructure-as-code AWS dédiée par eXalt IT à l'organisation des sessions de formation.<br><br>- [Julien GUISSET](https://www.linkedin.com/in/jguisset/), associé et co-fondateur d'eXalt IT. Julien a conçu l'articulation pédagogique de Git2Kube et sa déclinaison en projet ouvert sur Gitlab.|
|Projet Gitlab Git2Kube|Git2Kube est aussi un projet Gitlab en Creative Commons. [LICENCE.md](/LICENCE.md)|
| Victor, mainteneur | [Developer Relations Engineer](https://www.linkedin.com/in/victor-coatalem/) chez Exalt, en charge de la maintenance de la formation & de son industrialisation.