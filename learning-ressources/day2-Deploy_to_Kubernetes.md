---
layout: page
---

# Git2Kube - Jour 2/3 - Déployer vers Kubernetes

## Pré-requis du jour 2/3

- La réalisation de l'exercice confié à l'issue de la 1ère journée
- Des lectures pour se forger un premier verni sur Docker, Helm et Kubernetes.

> [Une bonne vidéo de IBM pour clarifier les idées fausses sur Kubernetes vs Docker](https://www.youtube.com/watch?v=2vMEQ5zs1ko&ab_channel=IBMTechnology), sous-titres français disponibles
___
### Pourquoi Kubernetes ?

- Les discussions du jour 1 montrent les qualités de Docker comme moyen de créer un *artéfact de déploiement* prêt à être utilisé. En aval, Kubernetes _(K8S pour les intimes)_ est la conséquence du choix de Docker.

- La sécurité, la notion d'environnements et d'isolation des processes.

- Le concept d'orchestration applicative

- hyper-densification au niveau processes et de mutualisation des ressources d'infrastructure.

___

### Quelques notions fondamentales :

**K8S est un orchestrateur de containers**

K8S permet de configurer l'instanciations d'images docker. Et c'est lui qui se charge du management des ressources d'exécutions.

A propos des **Containers** ils sont pensés pour n'exécuter qu'un seul service par container (un process et ses enfants). Le fait de multiplier des services décorélés au sein d'un même container complexifie le monitoring et le management des services.

A propos d'un **Pod** dans Kubernetes, plutôt que de déployer les containers individuellement, on déploie et on instancie des groupes de containers dépendants, colocalisés en une unité d'execution.

A propos d'un **Node** dans Kubernetes, c'est une machine de travail au sens qu'elle peut être un serveur virtuel ou physique, en fonction du cluster. Chaque nœud contient les services nécessaires à l'exécution de pods.

> La formation ne couvre pas le détail des objets Secrets, Deployments, Services, ... dans K8S. Pour vous faire une idée de la profondeur de l'outil, [cette petite infographie](https://www.reddit.com/r/kubernetes/comments/u9b95u/kubernetes_iceberg_the_bigger_picture_of_what_you/), _"Kubernetes iceberg"_: on reste sur la face émergée !

A noter :
- Deployment est l'objet qui va orchestrer la mise en ligne d'une version de notre applciation et sa mise à jour par la création de replicaset.
- Le role du replicaset est d'orchestrer l'instanciations des pods en fonction du run.
- Le nombre de pods / nodes permet de jouer sur l'hyper densification et d'optimiser l'emploi des ressources d'infrastructures.

![k8s logo](../media-assets/Kubernetes-Logo.jpg)

# Jour 2


##  Comment va-t-on demander à Helm de déployer l'image docker de notre application (mini-site nginx+artifact HTML/CSS) vers K8S ?

A l'issue de la première journée de formation et de l'exercice individuel, les 2 stages de build (``site-build`` et ``image-build``) ont été automatisés. 

La solution de l'exercice du premier jour de formation est [disponible ici](learning-ressources/_spoiler_solutions_exercices/.gitlab-ci-day1.yml). 

Le stage ``image-build`` de la pipeline a terminé par le dépôt de l'image de notre application dans la registry Gitlab du projet 
>_https://gitlab.com/git2kube/session-**{XXXXYY}**/**{repository}**/container_registry_


## Comment peut-on instancier l'image Docker stockée chez Gitlab dans l'environnement AWS Cloud9 ?


Le stage ``image-build`` se termine par le dépôt du container applicatif au sein de la registry Gitlab (``--destination $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA``)

Pour récupérer la référence de l'image dans la registry, se connecter sur Gitlab à la rubrique ``Packages and Registries > Container Registry`` puis retrouver dans la liste l'image enregistrée à l'issue du stage ``image-build`` de la pipeline.

Elle est référencée avec les variables ``$CI_COMMIT_SHORT_SHA``

Cliquez sur ``le Bloc note à droite de la référence de l'image`` pour récupérer l'adresse de l'image sur Gitlab et l'appeler en ligne de commande depuis Cloud9 :

```sh
docker run -p8080:80 [url d'accès l'image sur Gitlab]
Unable to find image 'registry.gitlab.com/git2kube/git2kube:478b83f6' locally                                                                 
docker: Error response from daemon: Get https://registry.gitlab.com/v2/git2kube/git2kube/manifests/478b83f6: denied: access forbidden.        
See 'docker run --help'.  
```
On constate que la connexion a echoué.

Il est en effet nécessaire de se créer un token d'accès afin de pouvoir se connecter à la registry Gitlab. 

Pour se faire, se connecter sur [son profil](https://gitlab.com/-/profile/personal_access_tokens) pour ajouter un access token.

Penser à le nommer de manière significative, par exemple ``Cloud9-RegistryCredential`` et à le limiter dans le temps et dans la portée, ici ``read_registry`` est suffisant.

Une fois le token créé, il s'emploie comme un mot de passe :

```sh
docker login registry.gitlab.com
```

Une fois  connecté, on peut relancer la commande :

```sh
docker run -p8080:80 [url d'accès l'image sur Gitlab]
```

Pour vérifier, utiliser ``Preview > Preview Running application`` dans l'environnement Cloud9.

Maintenant, on peut passer aux étapes de packaging et deploy.

___
## Création de la chart helm

Qu'est-ce que la chart ? 

C'est l'ensemble de la configuration Helm, une collection de fichiers yml variabilisés en cascade, qui permet de parametrer l'orchestration de l'application.

C'est un moteur de template de fichiers de configuration Kubernetes, ca simplifie en particulier la parité des environements qui ne diverge que de quelques variables.

Installation du kubectl [AWS Linux 2 Kubectl](https://docs.aws.amazon.com/eks/latest/userguide/install-kubectl.html)
```sh
mkdir -p $HOME/bin
curl -o $HOME/bin/kubectl https://amazon-eks.s3.us-west-2.amazonaws.com/1.21.2/2021-07-05/bin/darwin/amd64/kubectl
chmod +x /$HOME/bin/kubectl
```

> Si l'execution du binaire kubectl échoue, essayer avec ce lien: https://s3.us-west-2.amazonaws.com/amazon-eks/1.23.15/2023-01-11/bin/linux/amd64/kubectl


``kubectl version`` pour vérifier la version installée. 


> **kubectl** est l'outil de ligne de commande de K8S. Il permet d'executer des commandes dans les clusters K8S. 

Installation de [Helm](https://helm.sh/docs/intro/quickstart/)

```sh
curl -o helm.tgz https://get.helm.sh/helm-v3.8.0-linux-amd64.tar.gz
tar -zxf helm.tgz
mv linux-amd64/helm $HOME/bin/ 
```

On va maintenant paramétrer une première version de ``Chart.yml`` au sein d'un dossier dédié à la configuration de nos environnements Kubernetes :

```sh
helm create mini-site
mv mini-site chart
```
> Pourquoi a-t-on d'abord créé les templates avec ``mini-site`` pour renomer ensuite le répertoire en ``chart`` ?

Dans ``chart/templates/`` on retrouve des placeholders et templates préfaits, qui par exemple :
- embarquent au sein de  l'objet ``service`` une sorte de mini load balancer réseau devant les pods. C'est lui qui permettra la magie du rolling-deploy,
- ou encore l'objet ``ingress``, le fameux reverse proxy qui permet d'exposer en public l'application déployée.

Commande permmettant d'executer le moteur de template Helm en local :

```sh
helm template [xxx nom de release] chart
```
(warning sur la terminologie nom de release)

Paramétrage et personnalisation des values.
Que doit on modifier dans ``./chart/values.yml`` ( qui permet de factoriser les variables de configuration ) : le tag de l'image ?

```yml
image:
  repository: registry.gitlab.com/[votre projet]
  pullPolicy: IfNotPresent
  # Overrides the image tag whose default is the chart appVersion.
  tag: [remplacer ici par le tag référence de l'image sur Gitlab]
```

## Configuration des accès à EKS dans l'environment de formation

> EKS est le service kubernetes de AWS. EKS permet de créer des **clusters** dans le cloud. 1 cluster = _n_ machines prêtes à éxécuter des containers docker sur demande. Pour chaque session Git2Kube, Exalt met à votre dispotion un cluster EKS ([visible ici](https://eu-west-3.console.aws.amazon.com/eks/home?region=eu-west-3#/clusters)). 


Pour s'authentifier à EKS, on passe par la cli ``aws`` d'Amazon, installée par défaut sur l'instance Cloud9.

Pour l'instant les identifiants auto gérés par Cloud9 ne sont pas compatibles avec EKS.
Il faut donc se générer une clé d'accès avec AWS IAM :

[Via la console AWS](https://console.aws.amazon.com/iam/home?region=eu-west-3#/users), cliquez sur votre utilisateur puis ``Informations identification de sécurité > Créer une clé d'accès``

Vous obtenez une ID de clé d'accès et une Clé d'accès secrète ! __Ne fermez pas la fenêtre avant d'avoir récupérer la clé !__ :

```sh
aws configure --profile eks
```

Update du contexte et du .kubeconfig pour permettre à kubectl de fonctioner :

> Cette commande permet de connecter kubectl au cluster EKS qui tourne sur AWS, et donc d'éxécuter des commandes sur ce cluster.

```sh
aws eks update-kubeconfig --name [remplacer ici par le nom du cluster] --profile eks
``` 

Pour vérifier qu'on est en mesure de parler au cluster :

```sh
kubectl get all --namespace {my_trigram_lowercase}
``` 


Dans l'environnement de la session de formation, chacun des participants dispose de son namespace (ns en raccourci dans K8S). Il s'agit de votre trigramme (3 lettres) d'identification fourni par votre administrateur au lancement de votre formation.

La distinction des namespaces permet aux participants d'employer les même noms de ressources sur K8S, chacun dans son contexte. 

La commande ``kubectl config set-context --current --namespace=[my_trigram_lowercase]`` permet de ne pas avoir à préciser le namespace à chaque appel de kubectl.

(Quelques trucs et astuces à discuter le jour de la formation et qui facilitent les interactions avec K8S au quotidien : [kubectx](https://github.com/ahmetb/kubectx) et [kubens](https://github.com/ahmetb/kubectx/blob/master/kubens)

``helm ls --namespace=[my_trigram_lowercase]`` pour verifier que les accès sont bons au travers de helm et que rien n'est déployé pour le moment.

```sh
$ helm ls --namespace=[my_trigram_lowercase]
NAME    NAMESPACE       REVISION        UPDATED STATUS  CHART   APP VERSION
```

## Livraison de la Chart à K8S
  
Pour instancier Chart dans EKS, lancer la commande :

``helm install --wait mini-site chart/ --namespace=[my_trigram_lowercase]`` ou mini-site est le nom que nous choisissons de l'instanciation de la Chart.

La commande est failed ! Le pod a été créé mais ``ImagePullBackOff``.

[Effectivement il lui faut les crédentials Gitlab !](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/#create-a-secret-by-providing-credentials-on-the-command-line)

```sh
kubectl create secret generic gitlab --from-file=.dockerconfigjson=$HOME/.docker/config.json --type=kubernetes.io/dockerconfigjson --namespace=[my_trigram_lowercase]
kubectl get secrets --namespace=[my_trigram_lowercase]
```

Il faut maintenant ajouter la directive correspondante dans [values.yaml](./chart/values.yml) 

```yaml
imagePullSecrets:
  - name: gitlab
```
On peut maintenant retenter un ``helm install --wait mini-site chart/ --namespace=[my_trigram_lowercase]`` qui cette fois-ci fonctionne.

Si ``Helm`` répond OK, il précise l'url qui permet de vérifier.

Mais la section ``1. Get the application URL by running these commands:```est vide à ce stade.

Pour contourner, on va lancer un ``kubectl port-forward svc/mini-site 8080:80 --namespace=[my_trigram_lowercase]``

Et la ``Preview > Running Application`` fonctionne dans Cloud9.

On peut aussi constater l'état du cluster via [la console AWS](https://eu-west-3.console.aws.amazon.com/eks/home?region=eu-west-3#/clusters)  

Il est interressant de noter que la livraisons de la Chart a eu pour effet le déploiement des pods sur EKS...

C'est l'objet deployment qui a fait la magie (pas couvert par cette formation, mais on peut en parler).

Lancer un ``kubectl get all --namespace=[my_trigram_lowercase]`` pour le voir.


## Exercice individuel, automatiser le deployment de notre container depuis la pipeline Gitlab.

Il faudra  ajouter un nouveau stage ``deploy`` à [.gitlab-ci.yml](/.gitlab-ci.yml)

Mais, une fois de plus la principale difficulté consistera à résoudre la question des credentials et des environnements entre Gitlab et AWS.

On suggère de réfléchir autour d'un ``kubectl get all -A`` dans Cloud9.

Et Aux paramètres CI à employer dans les appels de commandes sur Gitlab.



### 🔥 -- Attention réponse ci-dessous, on essaie par soi-même avant -- 🔥


![spoiler cache](../media-assets/logo-Git2Kube.png)
![spoiler cache](../media-assets/logo-Git2Kube.png)
![spoiler cache](../media-assets/logo-Git2Kube.png)
![spoiler cache](../media-assets/logo-Git2Kube.png)

Vraiment ! vous pensiez qu'on allait vous donner la solution ? 



On va simplement vous donner un indice, ``gitlab-runner`` est un binaire fournit par GitLab que l'on peut déployer dans l'environnement souhaité.

On l'a fait pour vous sur l'infra AWS de la formation et notre runner privé est référencé sur Gitlab via le groupe des participants !

On peut voir les runners de groupe accessibles via [Gitlab](``https://gitlab.com/[remplacer ici par mon projet]/-/settings/ci_cd``)

Il suffit de vous assurer d'instancier le stage du ``deploy`` sur le bon runner, celui de l'environment ciblé ! 

Spoiler alert ! 

- Saurez-vous appeler le bon tag pour l'éxecution de la pipeline ?

- Notez par ailleurs que ce runner n'est pas admin du cluster EKS, vos commandes Helm doivent appeler explicitement votre namespace. 
