---

Vous êtes inscrit ou vous envisagez de vous inscrire à notre [prochaine session de certification](/training-sessions/session-abstract-0001-2021_xx-Git2Kube.md) ?  Vous trouverez sur cette page la liste des points à préparer en vue de participer à la une session Git2Kube.


# Comment dois-je préparer ma formation à distance ?

| Point à préparer | Description |
|:---|:---|
|Webcam activée|La collaboration à distance requiert, à défaut pour les participants d'être physiquement réunis, de pouvoir lire au travers de l'image le visage de ses interlocuteurs. Réunis en visio, les participants devront allumer leurs webcams pendant la formation. A ce titre, il doivent impérativement en disposer. |
|Casque et micro|La collaboration à distance requiert un son de qualité, c'est une marque de respect pour les autres. Un son qui sature est vite horripilant. Les participants veilleront pour leur confort et celui des autres à disposer d'un casque micro. Ils devront être certain à défaut de la qualité de leur prise de son.|
|Environnement calme|Pour leur confort et celui des autres participants, chacun devra s'assurer d'être installé pour la formation dans un environnement calme. Les espaces collectifs sont à proscrire.|
|Un accès Internet Stable de niveau fibre ou bonne ADSL.|Qu'il s'agisse de suivre l'expert et instructeur Git2Kube, d'échanger en visio ou d'accéder son environnement Cloud9 de développement... Une excellente connexion Internet est requise pour chacun des participants.|
|état d'esprit DevOps| Avant la formation, les participants sont invités à procéder à des recherches par leurs propres moyens sur la [culture devops](https://www.google.com/search?q=culture+devops) et l'état d'esprit qu'elle incarne. [12 factors](https://www.google.com/search?q=12+factors) et [Gitflow](https://www.google.com/search?q=gitflow) sont 2 autres points d'ancrage à préparer.|
|un compte Gitlab|Avec l'email d'inscription|
|Possibilité de réseauter à la pause déjeuner|Pour la pause déjeuner, les participants sont libres de s'organiser comme ils le souhaitent. Afin de favoriser le réseautage et les échanges, ceux qui le souhaitent pourront déjeuner ensemble "virtuellement", il conviendra pour eux d'avoir préparer leur "plateau repas" :)|
