---
layout: page
---

# Git2Kube - Jour 1/3 - Artifact build -  Image build

# Cloud9

Durant toute la formation, Cloud9 sera votre IDE de développement. Il s'agit d'un service de AWS qui permet d'avoir accès à un IDE dans le cloud, et donc dans le même réseau que les infrastructures déployées.

## Connexion à l'environement cloud9

[AWS Console](https://exalt-educ.signin.aws.amazon.com/console) -> [Cloud9](https://eu-west-3.console.aws.amazon.com/cloud9)

## Création des clés SSH dans l'environnement Cloud9

Créer une clé SSH dans l'environnement Cloud9.

```sh
$ ssh-keygen -t ed25519 -C "[your name]_ssh_key_cloud9"
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/ec2-user/.ssh/id_ed25519):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/ec2-user/.ssh/id_ed25519.
Your public key has been saved in /home/ec2-user/.ssh/id_ed25519.pub.
The key fingerprint is:
SHA256:oeCaHZAwIjqGYCgZCEnt4nPjU+7GOzVVDN0DCh6xu5M [your name]_ssh_key_cloud9
The key's randomart image is:
+--[ED25519 256]--+
|&*.    +..+.o    |
|&o o  . + .+ o   |
|=.+ .  o...   .  |
|.o + . ..o       |
|. . o ..S        |
| o * o oo        |
|  * * .E.        |
|   o =  .        |
|    +oo          |
+----[SHA256]-----+
```

Ajouter à votre compte [Gitlab](https://gitlab.com/-/profile/keys) associé à l'adresse email convenue à l'inscription.

## Git fork individuel du template de projet Git2Kube

Pour l'utilisation de Gitlab.com, un groupe a été mis en place: Session-XXXXYY, où XXXX désigne l'année en cours et YY le numéro de session.

Le compte Gitlab de chaque participant a été ajoutés au projet [Git2Kube template de la formation](https://gitlab.com/git2kube/git2kube)

Chacun doit maintenant forker ce projet pour créer sa copie individuelle de formation dans son groupe *Git2Kube* > Sous-Groupe de *session de formation* > _"prenom nom - Git2Kube"_

__Forker depuis l'interface dédiée de Gitlab [Fork Project Git2kube](https://gitlab.com/git2kube/git2kube/-/forks/new)__



Le partage des fork de repositories par participants est facilité et les participants d'une même sessions peuvent voir les projets des autres.

## Clone du projet dans l'environement Cloud9

Chaque participant clone son projet de site statique dédié.

_Remplacer par les bonnes varleurs!_

```sh
 git clone git@gitlab.com:git2kube/{prenom nom - }git2kube.git
```

## Configuration Git de l'environnement

_Remplacer par les bonnes varleurs!_

```sh
git config --global user.name "{PRENOM} {NOM}"
git config --global user.email "{emailCompteGitlab}"
```

## Principe du Learning by doing appliqué à Git2Kube

Bénéficiant chacun d'un repository individualisé, les apprenants vont l'enrichir au fil de la formation.

Ils sont invités à corriger et compléter les fichiers markdown du répertoire [learning-ressources](/learning-ressources/) avec leurs prises de notes.

Les participants ressortent de la formation avec leur repository, leurs exercices et leurs notes !

## Familiarisation avec Cloud9, Utiliser Tmux

Pour accéder à l'environnement de l'instructeur ou d'un autre participant : revenir au dashboard des environnements Cloud9 [menu burger > Shared with you]((https://eu-west-3.console.aws.amazon.com/cloud9/home/shared)) et entrer dans l'environnement souhaité.

Pour ouvrir le terminal que l'on souhaite suivre chez l'instructeur ou le participant : ``tmux ls`` puis ``tmux switch -t terminal_name``

Alternative, clic droit dans la console ``Tmux > Other sessions > ...``

## Installation Jekyll et Rendu du markdown en HTML

> Dans la formation Git2Kube, [Jekyll](https://jekyllrb.com/) est utilisé pour afficher le contenu Markdown de ce repo en HTML dans un navigateur. C'est surtout une bonne excuse pour créer un premier scénario de pipeline: la génération d'un site web statique en local ;)

Le dossier [jekyll](/jekyll/), contient la configuration de Jekyll pour générer un site statique à partir de l'ensemble des fichiers ``.md`` présents dans le repo.

Par rappport à une conf de base en particulier   ``source:..`` a été ajouté au fichier de configuration [_config.yml](/jekyll/_config.yml)

Installer ruby ``rvm reinstall ruby-2.7.5``

Puis depuis le répertoire [jekyll](/jekyll/) lancer :

``bundle install`` : installation des dépendances nécessaires à Jekyll.

Démarrage de Jekyll en mode serveur pour regénérer le contenu en live pendant les sessions de dev.
``bundle exec jekyll s``

Pour visualiser la Preview du rendu, dans la barre menu de Cloud9, ``Cliquez sur Preview > Preview`` running application.

Ou en simple commande, ``jekyll build``

Dans les deux cas, le dossier ``./_site`` du rendu est crée automatiquement.


## Automatisation du rendu du markdown dans la pipeline

Dans l'arborescence des fichiers de l'environement Cloud9, ToggleTree à Gauche, cliquez sur la roue des parametre puis ``Show hidden files`` .

Création du pipeline Gitlab : ``touch .gitlab-ci.yml`` à la racine du projet.

Warning : En yml comme en python, les premières tabulations font la structuration des objets.

Par convention du format yml, un objet yml débute par un ``---`` et termine par ``...``

On crée une première section ``stages:``

Ajout d'un premier stage à la pipeline `` - site-build``

Au sein d'un même stage, plusieurs jobs peuvent tourner en parralèle.

On crée le job ``jekyll:`` en précisant son stage d'appartenance, en l'occurence ``stage: site-build``.

On choisit une image de build adaptée aux dépendances. En l'occurence, une image ``ruby`` dans la version que nous souhaitons. Sans précision les images sont appelées depuis [Docker Hub](https://hub.docker.com/)

`` image: ruby:2.7`` (note de la rédaction pas de `latest` dans la version on choisit sa majeure!)

Puis on reprend les commandes que nous avions réaliseées en console. `` - bundle install`` et  `` - bundle exec jekyll build``

On termine en localisant le chemin de sortie ``- jekyll/_site`` de l'artifact dans une section ``artifacts:`` et sous-section ``paths:``

On obtient donc le fichier [.gitlab-ci.yml](/.gitlab-ci.yml) suivant :

```
---
stages:
  - site-build

jekyll:
  stage: site-build
  image: ruby:2.7
  script:
    - cd jekyll
    - bundle install
    - bundle exec jekyll build
  artifacts:
    paths:
      - jekyll/_site
...
```

Ensuite, on commit les ajouts et modifications vers le repo.

```sh
$ git add .gitlab-ci.yml  
```

(Pour voir l'état du dossier de travail)
```sh
$ git status  
```

On commit les modifications
```sh
$ git commit -m "description du commit"  
```

Et on pousse sur le répo !

```sh
$ git push  
```

Puis depuis Gitlab on observe que [La branche a bien été poussée](https://gitlab.com/{ici_chemin_repo}/-/branches) . Sans directive ``rules`` associée au ```stage``, il n'est pas nécessaire de merger dans le master pour que la pipeline s'exécute.

On retrouve dans [CI/CD > Pipelines](https://gitlab.com/{ici_chemin_repo]}/-/pipelines) la liste des jobs en train de tourner avec leurs états, l'accès aux logs de la console et finalement le téléchargement et la prévisualisation des artifacts.

> Si la pipeline ne se lance pas: Il se peut que le runner GitLab ( _Git2Kube > Session XXXXYY > Runners_ ) soit down, ou surchargé par d'autres utilisateurs. Dans ce cas, il est possible de lancer votre propre runner en local. https://docs.gitlab.com/runner/register/


A noter : c'est l'installation des dépendances sur l'image qui a consommé le plus de temps d'éxécution. Point d'attention pour les projets qui embarquent des dépendances et intérêt de préparer les images de build nécessaires et la mise en cache des dépendances.

## Création du container applicatif avec Docker

Une image docker est le tar.gz d'un système complet; il doit être le plus minimaliste possible par rapport au besoin et il sera exécuté dans un process isolé au niveau système sans recourir à la virtualisation.

> image docker = état initial (FROM) + altérations (COPY / ENV / RUN) + exécution d'un processus (CMD)
Dans un registry Docker, le stockage se fait par couches. Les couches déjà présentes sur des images Docker ne seront pas dupliquées si elles sont réutilisées par d'autres images.
> [une vidéo qui explique bien Image / Container / Registry en 5 minutes](https://www.youtube.com/watch?v=_dfLOzuIg2o&ab_channel=TechSquidTV)

Dans notre cas, il s'agit de propulser un site statique dans un environnement le plus simple possible avec une ``nginx alpine`` par exemple.

> Note: [Alpine](https://www.alpinelinux.org/) est une distribution Linux particulièrement légère & minimaliste, ce qui en fait un très bon choix de _**parent image (FROM)**_ de l'image qu'on cherche à construire 
> Rappel: Notre image est une archive (.tar/.gz) du système d'exploitation complet. + le système d'exploitation est léger, plus l'archive sera légère et facilement téléchargeable.

On définit les différentes étapes de construction d'une ou plusieurs images docker à partir d'un [Dockerfile](/.Dockerfile).

``touch Dockerfile``

Puis on ajoute au fichier les directives suivantes :

- choix de l'image d'origine : ``FROM nginx:stable-alpine`` ou ``nginx`` est une image disponible chez [Docker](https://hub.docker.com/) et ``stable-alpine`` la version désirée.

- copie de l'artifact, le site statique à la racine du serveur web : ``COPY jekyll/_site /usr/share/nginx/html``

```
FROM nginx:stable-alpine
COPY jekyll/_site /usr/share/nginx/html
```

Dans l'environnement Cloud9, le serveur Docker est lancé par défaut.

``ps -lef | grep docker`` pour s'en convaincre.

On peut donc lancer le ``docker build .`` en ligne de commande.

L'image est dorénavant présente sur le Docker local, faire un ``docker images`` pour vérifier.

Alternative, avec un `` docker build -t mini-site:latest .`` on nomme l'image et par convention, on ajoute un tag `latest` qui est un lien symbolique et ne peut pointer que vers une seule image du serveur docker, ici en local).

Refaire un  ``docker images``.

Enfin, pour lancer l'image en local faire un ``docker run --rm -p80:8080 mini-site:latest`` qui  réalise un mapping du ``:80`` local vers le ``:8080`` du container

> Fonctionellement, un container peut sembler très similaire à une VM. Cependant, sous le capôt c'est loin d'être la même chose. [Une super vidéo de IBM sur la différence entre VM et container](https://www.youtube.com/watch?v=cjXI-yxqGTI&ab_channel=IBMTechnology), sous-titres français disponibles.

Pour prévisualiser dans Cloud9 utiliser le bouton ``Preview`` en haut à droite de l'éditeur qui écoute sur le port ``:80``.

## Exercice pratique : Ajouter un stage image-build à la pipeline

Comment va-t-on builder l'image et l'enregistrer dans la [registry docker de notre projet Gitlab](https://gitlab.com/[mon_projet]/container_registry)...

(discussion philosophique sur : Pourquoi on passe par une registry plutôt que par un export tarball de l'image docker ``docker export mini-site:latest -o mini-site.tar`` ?)

L'intérêt de la registry : stocker les artifacts docker (les images) afin de les distribuer et de les historiser.

Exercice individuel, allez-y, ajoutez un stage image-build à la pipeline...

Bien évidemment, interdiction de commiter des credentials dans le repository...

En revanche il est possible d'employer [les variables d'environnement mises à disposition par Gitlab](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).

Si vous réussissez vous verrez apparaitre votre image dans la registry Gitlab avec pour tag de référence, votre commit.

Vous risquez d'avoir des refus, saurez-vous les surmonter...

```
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
error during connect: Post http://docker:2375/v1.40/auth: dial tcp: lookup docker on 169.254.169.254:53: no such host
```

### 🔥 -- Attention réponse ci-dessous, on essaie par soi-même avant -- 🔥


![spoiler cache](../media-assets/logo-Git2Kube.png)
![spoiler cache](../media-assets/logo-Git2Kube.png)
![spoiler cache](../media-assets/logo-Git2Kube.png)
![spoiler cache](../media-assets/logo-Git2Kube.png)

Vraiment ! vous pensiez qu'on allait vous donner la solution ?


```yml
docker:
  stage: image-build
  image: docker
  before_script:
    - docker login -u $CI_JOB_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - docker build -t registry.gitlab.com/[remplacer par username et projet]/mini-site:latest .
    - docker push registry.gitlab.com/[remplacer par username et projet]/mini-site:latest
```

Hélas Job failed dans la pipeline...

```sh
$ docker login -u $CI_JOB_USER -p $CI_JOB_TOKEN $CI_REGISTRY
"docker login" requires at most 1 argument.
See 'docker login --help'.
Usage:  docker login [OPTIONS] [SERVER]
Log in to a Docker registry
Cleaning up file based variables
00:01
ERROR: Job failed: exit code 1
```

Piste... La solution est ailleurs...

Trouverez vous la commande qui permet de construire des images docker sans passer par un serveur docker...
