---
layout: page
---

# Echanges sur la théorie du Build applicatif

> Cet article est un fil conducteur de discussion, qui introduit des notions autour du build et fournit des sources pour approfondir les sujets.




## Quelle est la finalité du build ?
* Construire, mettre à disposition des utilisateurs l'application/le service qu'ils attendent, qui soit quali et qui tourne bien
> "mettre à disposition des utilisateurs", ça peut être via des pipelines CI/CD qui automatisent la création d'une image docker, sa publication dans un registry, et son éxécution sur le cloud... ça peut aussi juste être un _Ctrl-C -> Ctrl-V_ d'un fichier HTML sur un filesystem de production.

* d'etre en capacité de le faire évoluer et de le maintenir avec le moins d'effort possible et avec une implication la plus large possible
> Si on reprend l'exemple ci-dessus, on voit bien qu'entre les deux manière de mettre à disposition le software aux utilisateurs, une est plus durable que l'autre.

* que le déploiement de l'architecture soit reproductible: meilleur moyen de rendre l'infrastructure auditable au niveau sécurité.

* Se mettre au service du produit, ca n'est pas renoncer, c'est mettre en place les bonnes collaborations.
* faciliter les itérations et les pivots





## Qu'est ce que le build : construction de l'application et sa mise à jour:

### 3 notions essentielles du build:
___
1. Contrôle de version

    Généralement, Git intégré à une plateforme de forge logicielle type GitHub / GitLab / Azure DevOps
    * environnement pour le process de build (GitLab-CI, GitHub Actions, ...)
    * capture les output des process de build
    * partage du code source à des machines distantes pour compilation.
    * Possibilité d'avoir plusieurs builds différents pour des versions différentes d'un même code source
    * tracabilité depuis le commit (1 build = 1 hash de commit)

    https://www.youtube.com/watch?v=xQujH0ElTUg&ab_channel=Atlassian

___

2. Contrôle qualité de code

    Test de l'application:
    * test unitaire
    * analyse de vulnérabilités
    * lint
    * etc...

___
3. Construction de l'artéfact de déploiement 

* artéfact de déploiement: image docker, nodes_modules, fat jar, ...
* il contient l'artéfact applicatif (php, war, HTML+CSS+js...)

* reconstruction depuis le code source : reproductibilité, résilience face à la panne + gestion des mises à jours, patchs de sécurité
* CI automatisée, unifiée et centralisée
* maitrise des dépendances, construction agnostique de l'environnement (fonctionne au delà de la machine du développeur)
* permet une traçabilité du commit jusqu'à l'environnement de test



![positionnement du build dans le processus de mise à disposition du code source](../media-assets/general-scheme.png)
_positionnement du build dans le processus de mise à disposition du code source_
___

## La build experience des participants
* Quelle est la finalité métier des applications que vous buildez ?
* Où identifiez-vous la complexité et les bottlenecks dans l'approche actuelle du build ?
* En termes de langages, de technos quels sont les principaux artifacts que vous buildez ?
* Avez vous des pipelines automatisés ? CI + CD ? 
* Avez-vous des dépôts d'artifacts ?
* Jenkins, Gitlab, Github, Bamboo, ... Quels outils ? On-prem ? en ligne ?
* Avez vous rencontré des situations avec over-engineering de la chaine de build ?

## Les choix de technos dans Git2Kube
* Faire un petit détours dans [understanding-Git2Kube-infrastructure-and-tools.md](/learning-ressources/understanding-Git2Kube-infrastructure-and-tools)
* Si vous êtes sages on vous donnera un appercu du repo Git2KubeInfra

