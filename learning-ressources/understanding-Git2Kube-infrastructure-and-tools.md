---
layout: page
---

# Besoins d'une infrastructure de formation

Git2Kube forme ses participants à la mise en place et à l'automatisation d'une chaîne de déploiement continue. Le prétexte est technique mais le fond est surtout méthodologique : devops, 12factors, gitflow, sécurité des secrets en production.

Chacune des sessions de formation mobilise de 6 à 10 participants qui doivent bénéficier d'un environnement moderne de développement, accessible en distanciel pour suivre leurs 3 jours de formation et travailler sereinement à l'acquisition de leur certification Git2Kube.

A chaque session, il est nécessaire de déployer :

- un cluster K8S configuré près à recevoir les pods des participants pour publier leur mini-sites sur un nom de domaine pré-définis
- un dépot Git dans gitlab.com
- des runners Gitlab
- et bien entendu un environnement dédié de développement pour chacun des participants.


# Orientations et contraintes d'infrastructure

Git2Kube émane d'eXalt IT qui dispose d'une infrastructure cloud AWS pour ses propres besoins (site corporate, projets internes, PoC des consultant).

Pour des raisons de praticité (administration sys et facturation), il a été décidé que les infrastructures dédiées aux formations Git2Kube seraient déployées au sein de l'organisation AWS eXalt IT.

A partir de là, les orientations et contraintes d'architecture étaient les suivantes :

- AWS as cloud provider,
- Chinese wall entre les ressources d'infrastructures corporate d'eXalt et les ressources des sessions de formation
- infra-as-a-service : l'infrastructure d'une session de formation doit être déployée pour l'occasion et détruite à l'issue de la formation,
- infra-as-code :le déploiement d'une infrastructure de formation doit être automatisé le plus possible et les dernières opérations manuelles documentées,
- concurrence des formations : plusieurs sessions de formation doivent pouvoir se tenir en simultané, pas de dépendance de ressources d'infrastructure.
- Coûts et empreinte carbone contenus : l'architecture est orientée par les coûts qui doivent être contenus sans que la qualité perçue des formations ne soit altérée. Les ressources d'infrastructures inutiles doivent s'éteindre automatiquement.


# Description générale de l'infrastructure dédiée à une promotion Git2Kube


|Point d'infrastrucure| Détail |
|:---|:---|
|AWS IAM | Gestion des identités des participants aux formations. Cohabitation avec le SAML/SSO Google Workspace d'eXalt. 2 Groupes de sécurité participant / admin |
| Service Catalog | Pour faciliter la gestion et le provisionning de Cloud9 : Service Catalog > Cloud 9 > System manager|
| Cloud9 | Environnement Cloud9 avec ses options de cost-saving et de connection via Systems manager (moins de config à gérer). Cloud9 provisionne instances de dev automatiquement|
|1 VPC / 1 Subnet public / 2 subnets privés / 1 nat gateway | Pour s'assurer du cloisonnement et de l'autonomie de chaque session de formation au sein de l'organisation AWS eXalt |
|Kubernetes via EKS|Une instance micro EKS par session<br><br>Suffisant et pratique pour les besoins de la formation|
|Gitlab| Un repository source contient le code source du mini projet.<br><br>Chaque participant dispose d'un projet individuel pour outiller sa chaîne de déploiement (Repository, Registry Docker, Pipelines)<br><br>Au niveau sécurité le groupe Git2Kube est indépendant des projets eXalt<br><br>Un nouveau sous-groupe est créé pour chaque nouvelle promotion de participants.<br><br>Les participant se connectent avec leurs comptes perso à Gitlab et gèrent leurs accès en SSH depuis leur instance de dev Cloud9.|
|Runners KUB| 1 runner qui écoutent le groupe Gitlab |

# Les dernières interventions manuelles

|Sujet|Nature de la manipulation|
|:---|:---|
| Discord |- Création d'un nouveau rôle/catégorie/channels par promotion de participants à la formation Git2Kube<br><br>- Ajout des participants|
| Gitlab | un groupe GIT2KUBE avec des sous-groupes par session de formation. Une convention de nommage |
