---
layout: page
---

# Git2Kube - Jour 3/3 - Automation du Release management

## Introduction au release management

Dans l'approche classique des montées de versions, la version déployée contient l'ensemble des nouveautés et correctifs validés par environnements successifs (dev > test > prod le plus souvent).

A l'opposé, dans le release management, les développeurs devraient : 

- aligner leur méthode de travail sur les métiers et les itérations du Product Management,

- avec une branche principale unique retraçant la vie de l'application, par opposition à des versions dictées par les environnements.

La pipeline doit refléter l'intention du Product managment (des releases fréquentes avec du sens et de l'impact pour les clients) et pas l'inverse (des années de mauvaises pratiques en silots).
 
L'important consiste à : penser Product, Features et Fixes puis de réaligner sa stratégie de branches en conséquence.
 
Les pipelines et la conteneurisation facilitent la création, le déploiement et la suppression d'environnements à la volée, au fil des flux de développement et des nécessités de recette.

En forçant le trait, on ne devrait avoir que l'environnement de production en fonctionnement permanent et tous les autres disponibles au besoin et montés à la volée.

## Gitflow et One Codebase

L'adoption du [Gitflow (A successful Git branching model, l'article original de Vincent Driessen)](https://nvie.com/posts/a-successful-git-branching-model/) permet aux équipes d'ancrer la notion de release sur la nécéssité produit, du hotfix à la version majeure.
 
En pariculier, le Gitflow :

- Facilite la propagation des fixes avec le merge down,
- Ne lie jamais une branche à un environment,
- Encourage la collaboration des équipes.
 
En asservissant les environments à la code base, on obtient le plein potentiel du Gitflow :

- philosophie devops, on partage la main (et la responsabilité sur l'infrastructure),
- les mises à jours de l'infrastructure sont liées au product management par rétroaction,

L'infrastructure, comme l'ensemble des modules qui partagent un cycle de vie identique partagent la même codebase.

Discussion philosophique sur la stratégie "one code base" et l'approche modulaire poussée à l'extrême (un cycle de vie produit = 1 repo)

(biais dans une organisation silotée, on utisera un 2nd repo synchronisé)

## Notion de release

La release n'est ni plus ni moins qu'un tag sur un commit.

Un tag à la nomenclature normée et qui matérialise la décision de livrer en production :

- Une section spécique est par exemple [consacrée dans Gitlab](https://gitlab.com/[Votre projet ici]/-/releases),
- La numérotation respecte généralement le formalisme [semver consacré](https://semver.org/),
- Voir le bon [article d'Hendrik Prinsza](https://hendrikprinsza.medium.com/gitflow-with-semver-dde625429aca)

## Concrétisation d'une stratégie de release dans la pipeline 

Au niveau de la pipeline, le release management consiste à conditionner tout ou partie de l'execution, en fonction de :
- l'origine du commit (fix, feature ...)
- et de la création d'une release (release candidate, snapshot, minor or major release...)

Dès lors qu'on respecte le Gitflow et la nomenclature semver, il suffit d'ajouter des conditions aux étapes du [.gitlab-ci.yml](../.gitlab-ci.yml)

## Premier ajout de règles

Pour l'esprit formation, projetons nous comme une équipe projet.
 
D'abord, cette équipe décide d'asservir le deploy aux seules release candidates souhaitées.

```yml
helm-to-eks:
  stage: deploy
  rules:
    - if: '$CI_COMMIT_TAG =~ /v[\d+]\.[\d+]\.[\d+]-rc.*/'
```
Ensuite, on peut imaginer ne pas vouloir containeriser systématiquement, uniquement pour les merges dans le master. 

(A noter, avec un seul dev par codebase pendant la formation nous n'avons pas de branche develop.)
 
```yml
kaniko:
  stage: image-build
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
```

Ce principe d'ajout de règles peut s'étendre à differentes notions : commit, tag, branches, merge requests...

Le mot clé ``environment`` permet de matérialiser plus facilement les opérations à partir ``Gitlab > Projet > Operations > Environnement``

C'est une convenance. Par exemple dans l'esprit d'une release candidate vers le staging, on va préciser :

```yml
helm-to-eks:
  environment: staging
  stage: deploy
  rules:
    - if: '$CI_COMMIT_TAG =~ /v[\d+]\.[\d+]\.[\d+]-rc.*/'
```

> Faire un commit du gitlab-ci.yml et visualiser la pipeline qui n'a plus que 2 étapes (Ne pas ajouter de tag de rc).


## Création d'une release candidate

En se reandant dans Gitlab, dans [la section release de chaque dépot]( https://gitlab.com/[votre projet ici]/-/releases).

Cliquer ``New Release`` puis dans Tag name > Create tag : ``v0.0.1-rc`` create from master dans release name ``v0.0.1-rc nom de version``

Observez que la pipeline se déclenche son intégralité. 

L'artefact du site-build n'est pas celui du container déployé (il est censé être identique), on peut ajouter une exclusion sur le site-build pour les plus pointilleux :

```yml
  stage: site-build
  rules:
    - if: '$CI_COMMIT_TAG !~ /v[\d+]\.[\d+]\.[\d+]-rc.*/'
```


## Création d'une release

En jouant sur les regexp (semver strict sans RC par exemple) on peut dupliquer l'étape ``helm-to-eks`` pour un déploiement de production.

Attention ci-dessous, tout se joue dans la personnalisation ``prod`` du déploiement vers EKS.

```yml
helm-to-eks-prod:
  stage: deploy
  rules:
    - if: '$CI_COMMIT_TAG =~ /v[\d+]\.[\d+]\.[\d+]$/'
  environment: production
  tags:
    - exalt4git2kube
  image: alpine/helm
  script:
    - helm upgrade --install --wait prod chart/ --set image.tag=$CI_COMMIT_SHORT_SHA --namespace [your trigramm]
```

Dans le même esprit, modifiez l'étape ``helm-to-eks`` en ``helm-to-eks-staging`` pour apporter la distincintion.

```yml
    - helm upgrade --install --wait staging chart/ --set image.tag=$CI_COMMIT_SHORT_SHA --namespace [your trigramm]
```

Au passage on adapte la condition du site-build pour éviter les builds d'artifacts orphelins (pas de nouveau commit de code quand on release)  :

```yml
jekyll  
  stage: site-build
  rules:
    - if: '$CI_COMMIT_TAG !~ /v[\d+]\.[\d+]\.[\d+].*/'
```

> Faire un commit du gitlab-ci.yml et visualiser la pipeline. 

Puis créer une nouvelle rc ``v0.0.2-rc`` avec l'ajout d'un tag.

Constatez un deploy vers l'environnement staging dans GitLab.

> Vérifier avec un ``kubectl get svc`` et une preview dans Cloud9 précédée d'un ``kubectl port-forward svc/staging-mini-site 8080:80``.

(A discuter, c'est toute la magie du ``helm create mini-site`` suivi du ``mv mini-site chart`` à la création de la chart qui s'exprime.)

> Via l'ajout d'un tag dans Gitlab, créer *enfin* la première release officielle ``v0.0.2`` à partir de la ``v0.0.2-rc``.

Vérifier avec un ``kubectl get svc``. En profiter pour supprimer le svc zombie avec un  ``helm delete mini-site``. 

Vérifier dans la preview Cloud9 précédé du  ``kubectl port-forward svc/staging-mini-site 8080:80`` ou ``prod-mini-site``.

## Bonus track #1

On peut ajouter ``--destination $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG`` au build de l'image dans la step image-build pour tager les images dans la registry GitLab.

Dans [Gitlab Operations > Environements](https://gitlab.com/[votre projet ici]/-/environments), on peut binder le ``helm delete``.

On vous laisse chercher :) Il existe aussi une clé ``auto_stop_in`` pour la step de ``stop``.

Le cas échéant on pourrait ajouter un `when : manual` pour la prod. 

(Vous imaginez les superpouvoirs que vous pourriez donner au Product Owner des projets ? One clic deploy :smile:)

## Bonus track #2 : publication sur Internet et exposition des services

C'est à la pipeline de respecter ton flow et pas ton flow d'être contraint !

On crée 2 fichiers supplémentaires dans la [chart](../chart/) : ``prod.yaml`` et ``staging.yaml``.

Dans la pipeline, on surcharge aussi les commandes ``helm`` avec des valeurs spécifiques qui conviennent : ``--values chart/prod.yaml`` ou ``--values chart/staging.yml``.

Pour la gestion du traffic et des routes dans chaque fichier par exemple :

```yml
ingress:
  hosts:
    - host: [trigramme][prod ou staging].[session de formation].git2kube.exalt-it.com
      paths:
      - path: /
```

puis l'activation de l'object ingress nginx dans le fichier ``values.yml``

```yml
ingress:
  enabled: true
  annotations:
    kubernetes.io/ingress.class: nginx
```

> Créer successivement une ``v0.0.3-rc`` et  ``v0.0.3`` !

Vérifier les ingress avec un ``kubectl get ing`` et appeler les routes en https (avec un s :smile:) !
 
Donnez libre cours à votre imagination... 

Est-ce que vous voyez venir la création d'environnements de recette montés à volée pour des feature branches en personnalisant l'ingress des svc avec le $CI_COMMIT_REF_SLUG ...
