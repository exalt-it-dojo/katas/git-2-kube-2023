---
layout: page
---

# Echanges sur la théorie du Deploy applicatif

## Tour de table sur le niveau de compréhension du deploy en 2/3 idées maximums.

## Quel est la finalité du déploiment

* Mettre à disposition la version d'une application à un groupe d'utilisateurs

## Réflexion sur les environnements

* Le déploiement regroupe la gestion/la cohérence des environnements de l'application

## Les clés du Déploiement

* Configuration de l'application
* Mise à jour de son environnement d'exécution
* Mise à jour du modèle de données
* Health check et qualification
* Rollout (ie routage du trafic)
* Traçabilité
* Rollout/Rollback
* Notification
* CD Automation


## La deploy experience des participants

* Est-ce que vous participez au deploy ? au run ?
* Le deploy est-il automatisé ?
* Avez-vous de la visibilité sur la partité des environnements ?
* Quels sont les principaux outils que vous utilisez ou que vous associez au deploy ?