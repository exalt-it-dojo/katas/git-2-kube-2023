---
layout: page
---

# Git2Kube Session v0
Une formation outillé avec [Cloud9](https://aws.amazon.com/fr/cloud9/)/[Gitlab](https://gitlab.com/)/[Docker](https://www.docker.com/)/[Helm](https://helm.sh/) et [AWS EKS](https://aws.amazon.com/fr/eks/)

Session des mardis 23 mars, 30 mars et 6 avril ! (3 jours - 6 participants).

> :smiley: Cette formation est complète !

## Introduction
La maîtrise de la chaîne de déploiement continue (Continuous Delivery) est un enjeu essentiel du cycle devops et de l’agilité. Plus largement, elle concourt au succès des projets IT et digitaux.

La formation Git2Kube forme ses participants à l’automatisation et l’industrialisation d’une chaîne de déploiement continue dans un environnement containeurisé. Elle met l'accent sur les enjeux du release management.

## Participant
Des profils techniques : Développeurs, Lead dev, Ingénieur système, Admin Sys
Des profils fonctionnels : Product Owner ou CDP qui touchent à la technique

## Pré-requis
Connaissances de base en développement logiciel, en ops ou run des infrastructures.
Ne pas avoir peur d’utiliser la ligne de commande, on va manipuler du Shell, AWS Cli, Git,ssh...
Doit savoir évoluer dans un environnement de développement ; une console ; un repo.

## Objectifs pédagogiques
Automatiser une chaîne de déploiement CI/CD; de Git vers un environnement orchestré (Kubernetes).
Apprentissage des concept clé du release managment.
Méthodologies : Git Flow / 12 factors / Semver / Devops
Principaux outils : Gitlab / Docker / Helm  / AWS EKS

## Méthode pédagogique
La formation est dispensée sur 3 mardi consécutifs. Les participants se connectent en distanciel et travaillent dans un environnement partagé et sécurisé (serveur Discord et infrastructure AWS dédiée à la formation).

A l’issue des 2 premières journées de formation, les participants doivent chacun réaliser des travaux individuels pour la session suivante. La formation représente en conséquence: 21h de formation auxquelles s’ajoutent 4h à 8h d’exercices individuels en fonction du niveau du participant.

Les travaux pratiques représentent plus de 50% de la formation. A cela s’ajoute 40% de théorie et 10% d’échanges entre les participants. Un cas concret sera déroulé sur l’ensemble journée et les participants déploieront un site web.

## Expert et formateur
L'expert et formateur de cette session est [Nicolas Lacourte](https://www.linkedin.com/in/nicolaslacourte/), concepteur et co-auteur de la formation.

### CV de Nicolas
Admin Sys / SRE et Architecte Cloud de 20 ans d’expérience.

**Principaux rôles** :
- Lab manager eXalt, co-auteur de Git2Kube
- Architecte Senior STORENGY 2020/en cours
- Consultant sénior Infrastructure/DevOps : France Télévision, Interencheres, Doctolib, Edelia...
- Responsable Infra Car & Boa tMedia (2008/2011)
- Technicien réseau Armée de l'air et de l'espace.

**Formations initiale** : Université de Cergy (DUT 1999), CNAM (Master 2007)

## Agenda de la formation
3 jours de formation : les mardis 23 mars, 30 mars et 6 avril.
- Les matins de 8h30 - 12h30
- Les après-midi de 14h à 17h00

Travail individuel à l'issue des 1ère et 2nde journées, prévoir de ~2 à 4h par exercice.

### Journée 1
- Présentation de la formation et des objectifs pédagogiques
- [Concepts généraux du Build (2h)](/learning-ressources/day1-BuildTheory.md)
- Ice breaker : présentation des parcours de chacun + environnement de travail + écosystème professionnel (environnement, stack, methodo, outils)
- Pourquoi nos choix d'outillage pour Git2Kube ?
- [Pratique : build de l'artifact, build de l'image docker.](learning-ressources/day1-Artifact_build_Image_Build)
- Exercice individuel : automatisation du build au sein de la pipeline (.gitlab-ci.yml)

### Journée 2
- Revue par les pairs des travaux individuels (1h)
- [Concept généraux sur le Deploy (2h)](/learning-ressources/day2-DeployTheory.md)
- [Pratique : contraintes multi-environnements (Gitlab/AWS), Orchestration, Livraison à Kubernetes (Chart/helm)](learning-ressources/day2-Deploy_to_Kubernetes.md)
- Exercice individuel : automatisation du deploy au sein de la pipeline

### Journée 3
- Revue par les pairs des travaux individuels (1h)
- [Pratique : GitFlow, rules & déploiement d'environnements à la volée](/learning-ressources/day3-Release_Management.md)
- Travaux libres sur les pipelines (stop, auto-stop, feature-branch slug release)

## Supports pédagogiques
- Un environnement de projet et de développement sécurisé clé en main : Gitlab + AWS + Cloud9

- Un serveur vocal Discord sera mis à disposition, sans interruption du début à la fin de la formation (Il permettra la tenue des 3 journée de session en distanciel. Il servira de serveur d’entraide pour les participants entre 2 sessions.)

- [Un repository Git reprenant les exercices pratiques des sessions de formation et annexant les solutions commentées des 2 exercices individuels.](https://gitlab.com/git2kube/git2kube)
